﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Dax_LabelPrinter.DataContext
{
    public class NavCom
    {
        public static Dictionary<string, string> ValuesCache = new Dictionary<string, string>();

        public static string ProcesNumSeries(ref string queryString)
        {
            string pattern = @"(?<whole>@\*NumSeries\*((?<param>[ 0-9a-zA-Z_-]+)))";
            Regex regex = new Regex(pattern);
      

            foreach (Match m in regex.Matches(queryString))
            {
                string paramName = m.Groups["param"].Value;
                if (!ValuesCache.ContainsKey(paramName)) //prevent multiple SSCC numbers on one label.
                {
                    using (DaxSrv_Produkcja.ProdukcjaCodeunit_PortClient client = new DaxSrv_Produkcja.ProdukcjaCodeunit_PortClient())
                    {
                        var val = client.GetNextSeriesNo(paramName);
                        queryString = Regex.Replace(queryString, pattern, val.Trim());
                        var checkDigit = Hekpers.CheckSumCalc.CalcGs1_Checksum_Digit(queryString);
                        queryString += checkDigit;
                    }
                    ValuesCache.Add(paramName, queryString);
                }
                else queryString = ValuesCache[paramName];
            }
            
            return queryString;
        }
    }
}
