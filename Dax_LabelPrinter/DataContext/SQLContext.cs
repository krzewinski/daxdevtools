﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace Dax_LabelPrinter.DataContext
{
    public class SQLContext : Context
    {
        string _query;

        public string Query { get { return _query; } set { _query = value; parseParameters(); ChangeProperty("Query"); } }

        public List<SqlParameter> SqlParameters { get { return _sqlParameters; } set { _sqlParameters = value; } }

        public List<string> Parameters = new List<string>();
        List<SqlParameter> _sqlParameters;

        void parseParameters()
        {
            this._query = QueryParserLib.ParametersParser.ProcesNumSeries(this.Query);
            this.Parameters = QueryParserLib.ParametersParser.GetParameterNames(this.Query);




        }
        byte[] _appRoleEnableCookie;
        private bool ExecuteDisableAppRole(string procName, out string msg, SqlConnection _sqlConn)
        {
            msg = "";
            bool result = true;
            SqlCommand cmd = new SqlCommand(procName);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = _sqlConn;
            SqlParameter paramEnableCookie = new SqlParameter();
            paramEnableCookie.Direction = ParameterDirection.Input;
            paramEnableCookie.ParameterName = "@cookie";
            paramEnableCookie.Value = this._appRoleEnableCookie;
            cmd.Parameters.Add(paramEnableCookie);

            try
            {
                cmd.ExecuteNonQuery();
                this._appRoleEnableCookie = null;
            }
            catch (Exception ex)
            {
                result = false;
                msg = "Could not execute disable approle proc." + Environment.NewLine + ex.Message;
            }

            return result;
        }
        private bool ExecuteEnableAppRole(string procName, out string msg, SqlConnection _sqlConn)
        {
            //https://stackoverflow.com/questions/290652/get-output-parameter-value-in-ado-net

            msg = "";
            bool result = true;
            SqlCommand cmd = new SqlCommand(procName);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = _sqlConn;
            SqlParameter paramAppRoleName = new SqlParameter();
            paramAppRoleName.Direction = ParameterDirection.Input;
            paramAppRoleName.ParameterName = "@rolename";
            paramAppRoleName.Value = "DAX_ZPL_PRINTING";
            cmd.Parameters.Add(paramAppRoleName);

            SqlParameter paramAppRolePwd = new SqlParameter();
            paramAppRolePwd.Direction = ParameterDirection.Input;
            paramAppRolePwd.ParameterName = "@password";
            paramAppRolePwd.Value = "Jan#wal1";
            cmd.Parameters.Add(paramAppRolePwd);

            SqlParameter paramCreateCookie = new SqlParameter();
            paramCreateCookie.Direction = ParameterDirection.Input;
            paramCreateCookie.ParameterName = "@fCreateCookie";
            paramCreateCookie.DbType = DbType.Boolean;
            paramCreateCookie.Value = 1;
            cmd.Parameters.Add(paramCreateCookie);

            SqlParameter paramEncrypt = new SqlParameter();
            paramEncrypt.Direction = ParameterDirection.Input;
            paramEncrypt.ParameterName = "@encrypt";
            paramEncrypt.Value = "none";
            cmd.Parameters.Add(paramEncrypt);

            SqlParameter paramEnableCookie = new SqlParameter();
            paramEnableCookie.ParameterName = "@cookie";
            paramEnableCookie.DbType = DbType.Binary;
            paramEnableCookie.Direction = ParameterDirection.Output;
            paramEnableCookie.Size = 1000;
            cmd.Parameters.Add(paramEnableCookie);

            try
            {
                cmd.ExecuteNonQuery();
                SqlParameter outVal = cmd.Parameters["@cookie"];
                // Store the enabled cookie so that approle  can be disabled with the cookie.
                 this._appRoleEnableCookie = (byte[])outVal.Value;
            }
            catch (Exception ex)
            {
                result = false;
                msg = "Could not execute enable approle proc." + Environment.NewLine + ex.Message;
            }

            return result;
        }
        void UnSetAppRole(SqlConnection conn)
        {
            try
            {
                string errMsg;
                ExecuteDisableAppRole("sys.sp_unsetapprole", out errMsg, conn);
                if (!String.IsNullOrEmpty(errMsg)) throw new Exception(errMsg);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
            }

        }
        void SetAppRole(SqlConnection conn)
        {
            try
            {
                string errMsg;
                ExecuteEnableAppRole("sys.sp_setapprole", out errMsg, conn);
                if (!String.IsNullOrEmpty(errMsg)) throw new Exception(errMsg);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
            }

        }
        public DataTable RunQuery()
        {
            try
            {
                if ((this.Query != null) && (this.SqlParameters != null))
                {
                    using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.Brain))
                    {
                        conn.Open();
                        SetAppRole(conn);
                        SqlCommand cmd = new SqlCommand(this.Query, conn);
                        cmd.Parameters.AddRange(this.SqlParameters.ToArray());
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);

                        DataTable dt = new DataTable();
                        adp.Fill(dt);
                        UnSetAppRole(conn);
                        return dt;
                    }


                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
                System.Diagnostics.Debug.Fail(ex.Message);

            }
            return null;

        }



    }
}
