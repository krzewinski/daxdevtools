﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Threading;

namespace Dax_LabelPrinter.DataContext
{
    public class Context : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void ChangeProperty(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }
    }
    public class DaxContext : Context
    {
        public DaxContext()
        {
            //przeniesc pluginy gdzie indziej najlepiej do tsqla le nie mam czasu teraz sie tym bawić;
            Hekpers.VariableReader_Helper.OnReplaceVariableValue += VariableReader_Helper_OnReplaceVariableValue;
            Hekpers.VariableReader_Helper.OnReplaceStart += VariableReader_Helper_OnReplaceStart;
        }

        private void VariableReader_Helper_OnReplaceStart()
        {
            NavCom.ValuesCache.Clear();
        }

        private void VariableReader_Helper_OnReplaceVariableValue(ref string obj)
        {
            NavCom.ProcesNumSeries(ref obj);
            
        }

        

        string _status;

        string labelText = "";
    
        ObservableCollection<DataContext.DaxZPLVariable> variables;
        //string labelPrinterIP = "192.168.236.141";
        string labelPrinterIP = "192.168.10.218";
        public string LabelText
        {
            get
            {

                return labelText;
            }

            set
            {

                labelText = value;
                var t = Dax_LabelPrinter.Hekpers.VariableReader_Helper.ReadVariables(labelText);

                MergeVariables(t);
                ChangeProperty("LabelText");
            }
        }
        void MergeVariables(List<DaxZPLVariable> list)
        {


            foreach (var v in list)
            {
                if (!this.Variables.Where(x => x.Name == v.Name).Any())
                {
                    this.Variables.Add(v);
                    ChangeProperty("Variables");
                }
            }
            List<DaxZPLVariable> toDel = new List<DaxZPLVariable>();

            foreach (var v in this.Variables)
            {
                if (!list.Where(x => x.Name == v.Name).Any()) toDel.Add(v);
            }
            foreach (var v in toDel)
            {
                this.Variables.Remove(v);
            }
            toDel = null;
        }


        public ObservableCollection<DaxZPLVariable> Variables
        {
            get
            {
                if (this.variables == null)
                {
                    this.variables = new ObservableCollection<DaxZPLVariable>();
                    ChangeProperty("Variables");
                }
                return variables;
            }

            set
            {
                variables = value;
                ChangeProperty("Variables");
            }
        }

        public string LabelPrinterIP
        {
            get
            {
                return labelPrinterIP;
            }

            set
            {
                labelPrinterIP = value;
                ChangeProperty("LabelPrinterIP");
            }
        }

        public string LabelToSend
        {
            get
            {
                var str = Hekpers.VariableReader_Helper.Replace(this.labelText, this.Variables);
                return str;

            }
        }

        public string Status { get { return _status; } set { _status = value;
                ChangeProperty("Status");
            } }


        public async Task Print_Async()
        {
                    System.Threading.ThreadPool.QueueUserWorkItem((x)=>
                    {
                        Print();
                    }
                );
            
        }

        public void Print()
        {

            DaxZPL_Lib.ZPL zpl = new DaxZPL_Lib.ZPL(this.LabelPrinterIP);
            //System.Diagnostics.Debug.WriteLine(this.LabelToSend);
           
            zpl.SendZplFile(this.LabelToSend, 65001);
            
        }
    }

    public class Dax_Base : Object, System.ComponentModel.INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void Notify(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }
    }


    public class DaxZPLVariable : Dax_Base
    {

        string name;
        string value;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                Notify("Name");
            }
        }

        public string Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
                Notify("Value");
            }
        }


        public DaxZPLVariable(System.Text.RegularExpressions.Match m)
        {

            var g = m.Groups["name"];
            this.Name = g.Value;




        }


    }

}

