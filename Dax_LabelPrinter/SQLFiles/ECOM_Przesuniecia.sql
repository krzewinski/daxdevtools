﻿select i.[EAN13 Code] [KOD_EAN13]
,  FORMAT(ile.[Expiration Date],'yyMMdd') [DATA_WAZNOSCI]
,cast(ile.Quantity as float) [ILOSC]
,ile.[Lot No_] [LOT]
--,'@!NumSeries!M-SSCC GS1'+ dbo.CalculateCheckDigit('@!NumSeries!M-SSCC GS1') [NR_SSCC]
,'@*NumSeries*M-SSCC GS1' [NR_SSCC]
,'Omnipack Sp. z o.o.' [CONTRACTOR_NAME_T]
,'00-848 Warszawa' [CONTRACTOR_ADR_1_T]
,'ul. Żelazna 59' [CONTRACTOR_ADR_2_T]
,cast(case when iuom.[Qty_ per Unit of Measure]<>0 then cast(ile.Quantity as int)/cast(iuom.[Qty_ per Unit of Measure] as int) else 0 end as float) [ILOSC_NAKLEJEK]

 from [Dax$Transfer Shipment Header]h
join [Dax$Transfer Shipment Line] l on l.[Document No_] =  h.No_
join [Dax$Item] i on i.No_= l.[Item No_]
left outer join [Dax$Item Unit of Measure] iuom on iuom.[Item No_]=l.[Item No_] and iuom.Code ='KARTON' 
--left outer join [Dax$Item Entry Relation] lots on lots.[Source ID]=l.[Document No_] and lots.[Source Ref_ No_] = l.[Line No_] and lots.[Source Type]=5745
left outer join [Dax$Item Ledger Entry] ile on ile.[Entry Type]=4 and ile.[Document Type]=9 and ile.[Document Line No_]=l.[Line No_] and ile.[Document No_]=l.[Document No_] and ile.[Location Code]='W DRODZE'

where h.[Transfer-to Code] = 'ECOM'
and l.Quantity<>0
and l.[Document No_]=@Nr_Dokumentu