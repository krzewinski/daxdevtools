﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;


namespace Dax_LabelPrinter.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy AutoSQL.xaml
    /// </summary>
    public partial class AutoSQL : Window
    {
        DataContext.DaxContext context;
        DataContext.SQLContext sqlContext = new DataContext.SQLContext();

        public AutoSQL(DataContext.DaxContext _context)
        {
            this.context = _context;
            InitializeComponent();
        }



        private void OpenSql_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Pliki zapytań (*.sql)|*.sql|All files (*.*)|*.*";
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(),
                "SQLFiles");
                if (dialog.ShowDialog() == true)
                {
                    System.IO.FileInfo fi = new System.IO.FileInfo(dialog.FileName);
                    if (!fi.Exists) return;
                    var rd = fi.OpenText();
                    this.sqlContext.Query = rd.ReadToEnd();

                    Windows.SQLParamsWindow paramsToFill = new SQLParamsWindow(this.sqlContext.Parameters);
                    paramsToFill.ShowDialog();

                    this.sqlContext.SqlParameters = paramsToFill.Parameters;

                    var dt = this.sqlContext.RunQuery();
                    this.dataGrid.AutoGenerateColumns = true;
                    this.dataGrid.ItemsSource = dt.DefaultView;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(String.Format("exception:{0},{1},{2}", ex.Message, ex.StackTrace, ex.InnerException));
            }

            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGrid.SelectedItem == null)
            {
                MessageBox.Show("Zaznacz pozycję!");
                return;
            }
            FillvVariablesAndPrint((System.Data.DataRowView)this.dataGrid.SelectedItem);


        }
        public enum PrintType { Normal, Image }
        void FillvVariablesAndPrint(System.Data.DataRowView item)
        {
            FillvVariablesAndPrint(item, PrintType.Normal);
        }
        void FillvVariablesAndPrint(System.Data.DataRowView item, PrintType type)
        {
            foreach (var v in this.context.Variables)
            {

                if (item.Row.Table.Columns.Contains(v.Name.Trim()))
                {
                    v.Value = item[v.Name.Trim()] != null ? item[v.Name.Trim()].ToString() : "";
                }
            }
            switch (type)
            {
                case PrintType.Normal:
                    
                    this.context.Print();
                    break;
                case PrintType.Image:
                    var imageName = Hekpers.ZPLPreview.PostZplAndReturnImageName(this.context.LabelToSend, this.context.LabelPrinterIP);
                    var image = Hekpers.ZPLPreview.LoadImageFromPrinter(imageName, this.context.LabelPrinterIP);
                    FileInfo fi = new FileInfo(System.IO.Path.GetTempFileName());

                    var fn =System.IO.Path.Combine(@"c:\1\1\", fi.Name);
                    image.Save(fn);
                    
                    break;
            }
            

        }

        private void Button_printAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in this.dataGrid.Items)
            {
                if (item is System.Data.DataRowView)
                    FillvVariablesAndPrint((System.Data.DataRowView)item);
            }
        }

        private void Button_SaveImages_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in this.dataGrid.Items)
            {
                if (item is System.Data.DataRowView)
                    FillvVariablesAndPrint((System.Data.DataRowView)item,PrintType.Image);
            }
        }

        private void Button_SaveImageCurrent_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGrid.SelectedItem == null)
            {
                MessageBox.Show("Zaznacz pozycję!");
                return;
            }
            FillvVariablesAndPrint((System.Data.DataRowView)this.dataGrid.SelectedItem,PrintType.Image);
        }
    }
}
