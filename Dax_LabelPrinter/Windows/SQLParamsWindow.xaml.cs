﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.ComponentModel;

namespace Dax_LabelPrinter.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy SQLParamsWindow.xaml
    /// </summary>
    public partial class SQLParamsWindow : Window, System.ComponentModel.INotifyPropertyChanged
    {
        List<SqlParameter> parameters;

        public event PropertyChangedEventHandler PropertyChanged;

        void change(string prop)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(prop));

        }
        public List<SqlParameter> Parameters
        {
            get { return parameters; } set { parameters = value; change("Parameters"); }
        }

        public SQLParamsWindow(List<string> parameters)
        {
            this.Parameters = parameters.Select(x => { return new SqlParameter() { ParameterName = x, Value = "" }; }).ToList();
            this.DataContext = this;
            InitializeComponent();
            

        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();

        }
    }
}
