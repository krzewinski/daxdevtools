﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Dax_LabelPrinter.Hekpers
{
    public delegate void ActionRef<T>(ref T obj);
    public static class VariableReader_Helper
    {
        public static event ActionRef<string> OnReplaceVariableValue;
        public static event Action OnReplaceStart;
        public static string Pattern
        {

            get { return "(?<var>#V(?<name>.*?)#V)"; }
        }
        static void onReplaceStart()
        {
            if (OnReplaceStart != null) OnReplaceStart();
        }
        static void onReplace(ref string v)
        {
            if (OnReplaceVariableValue != null) OnReplaceVariableValue(ref v);
        }
        public static string Replace(string input, IEnumerable<Dax_LabelPrinter.DataContext.DaxZPLVariable> var)
        {
            onReplaceStart();
            input = Regex.Replace(input, Pattern,
                m =>
                {
                    var g = m.Groups["name"];
                    var v = var.Where(x => x.Name == g.Value).FirstOrDefault();
                    if (v != null)
                    {
                        string varValue = v.Value;
                        if (varValue == null) varValue = "";
                        onReplace(ref varValue);
                        return varValue;
                    }
                    return "";
                }
                );
            return input;

        }
        public static List<Dax_LabelPrinter.DataContext.DaxZPLVariable> ReadVariables(string str)
        {
            List<Dax_LabelPrinter.DataContext.DaxZPLVariable> list = new List<DataContext.DaxZPLVariable>();
            Regex regx = new Regex(Pattern);
            var matches = regx.Matches(str);
            foreach (Match m in matches)
            {
                list.Add(new DataContext.DaxZPLVariable(m));

            }

            return list;

        }
    }
}
