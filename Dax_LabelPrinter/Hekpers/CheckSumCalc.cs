﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dax_LabelPrinter.Hekpers
{
    public class CheckSumCalc
    {

        public static string CalcGs1_Checksum_Digit(string input)
        {
            var arr = input.Reverse().ToArray();
            var sum = 0;
            var multip = 1;
            for (int i = 0; i < arr.Count(); i++)
            {
                if ((i + 1) % 2 == 0) multip = 1;
                else multip = 3;
                if (Char.IsDigit(arr[i]))
                {
                    sum += multip * (int) Char.GetNumericValue(arr[i]);
                }

            }

            var ret = sum % 10;
            if (ret != 0) ret = 10 - ret;
            return ret.ToString();

        }
    }
}
