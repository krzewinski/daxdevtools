﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace Dax_LabelPrinter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataContext.DaxContext context = new Dax_LabelPrinter.DataContext.DaxContext();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this.context;
            this.context.LabelText = "Tu wpisz definicję ZPL";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //DaxZPL_Lib.ZPL zpl = new DaxZPL_Lib.ZPL(this.context.LabelPrinterIP);

            //System.IO.FileInfo fi = new System.IO.FileInfo(@"C:\1\zpl.txt");
            //zpl.SendZplFile(fi, 65001);
            // zpl.SendZplFile(this.context.LabelToSend, 65001);
            this.context.Print();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Open_MI_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter= "Pliki naklejek (*.zpl)|*.zpl|All files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            dialog.InitialDirectory = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(),
            "ZplFiles");
            if (dialog.ShowDialog()==true)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(dialog.FileName);
                if (!fi.Exists) return;
                var rd = fi.OpenText();
                this.context.LabelText = rd.ReadToEnd();

            }

        }

        private void Save_MI_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Pliki naklejek (*.zpl)|*.zpl|All files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            dialog.InitialDirectory = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(),
            "ZplFiles");
            if (dialog.ShowDialog() == true)
            {
                File.WriteAllText(dialog.FileName, this.context.LabelText);

            }
        }

        private void ImportSQL_Click(object sender, RoutedEventArgs e)
        {
            Windows.AutoSQL sqlWindow = new Windows.AutoSQL(this.context);
            sqlWindow.ShowDialog();
        }

        private void BtnPreview_Click(object sender, RoutedEventArgs e)
        {
            var imageName = Hekpers.ZPLPreview.PostZplAndReturnImageName(this.context.LabelToSend, this.context.LabelPrinterIP);
            var image = Hekpers.ZPLPreview.LoadImageFromPrinter(imageName, this.context.LabelPrinterIP);
            Windows.ImagePreview previewWindow = new Windows.ImagePreview(image);
            previewWindow.Show();
        }
    }
}
