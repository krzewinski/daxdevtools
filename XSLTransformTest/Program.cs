﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.IO;

namespace XSLTransformTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string outputFile=String.Format("output{0}.html", DateTime.Now.ToFileTime());
            XslCompiledTransform Xt = new XslCompiledTransform(true);
            Xt.Load(@"arkusz.xslt");
            XmlDocument xd = new XmlDocument();
            xd.Load(@"zrodlo.xml");
            FileStream ms = new FileStream(outputFile,FileMode.Create);

            Xt.Transform(xd, null, ms);
            ms.Flush();
            ms.Close();
            FileInfo fi = new FileInfo(outputFile);
            if(fi.Exists==false) return;
            StreamReader sr= fi.OpenText();
            string str = sr.ReadToEnd();
            sr.Close();
            //ms.Position = 0;//powroit inaczej nic nie przeczyta;
            
            //StreamReader rdr = new StreamReader(ms);

            //Console.Write(rdr.ReadToEnd());
        }
    }
}
