﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="msxsl"
xmlns:ms="urn:schemas-microsoft-com:xslt"
xmlns:dt="urn:schemas-microsoft-com:datatypes"
xmlns:foo="http://whatever"
>
  <xsl:output method="xml" encoding="UTF-8" />

  <xsl:function name="foo:compareCI">
    <xsl:param name="string1"/>
    <xsl:param name="string2"/>
    <xsl:value-of select="compare(upper-case($string1),upper-case($string2))"/>
  </xsl:function>

  <xsl:template match="/">
    <xsl:element name="Invoice">

      <xsl:apply-templates select="Evenlope/Data" mode="Header" />
      <xsl:apply-templates select="Evenlope/Data" mode="Parties" />
      <xsl:apply-templates select="Evenlope/Data" mode="Lines" />
      <xsl:apply-templates select="Evenlope/Data" mode="Footer" />

    </xsl:element>
  </xsl:template>

  <xsl:template name="InvoiceParties" match="Data" mode="Parties" >
    <xsl:element name="InvoiceParty">

      <xsl:element name="OrderParty">
        <xsl:element name="BuyerOrderNumber">
          <xsl:value-of select="S[@n='Order']/L/F[@n='BuyerOrderNumber']"/>
        </xsl:element>
        <xsl:element name="BuyerOrderDate">
          <xsl:if test="string-length(S[@n='Order']/L/F[@n='BuyerOrderDate'])&gt;0">
            <xsl:value-of select="S[@n='Order']/L/F[@n='BuyerOrderDate']"/>
          </xsl:if>
          <xsl:if test="not (string-length(S[@n='Order']/L/F[@n='BuyerOrderDate'])&gt;0)">
            <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoiceDate']"/>
          </xsl:if>
        </xsl:element>
      </xsl:element>

      <xsl:if test="string-length(S[@n='Delivery']/L/F[@n='DeliveryLocationNumber'])&gt;0">
        <xsl:element name="DeliveryParty">
          <xsl:element name="DeliveryDate">
            <xsl:value-of select="concat(S[@n='Delivery']/L/F[@n='DeliveryDate'],'T12:00:00')"/>
          </xsl:element>
          <xsl:element name="DeliveryDocumentNumber">
            <xsl:value-of select="S[@n='Delivery']/L/F[@n='DespatchNumber']"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>

      <xsl:element name="BuyerParty">
        <xsl:element name="ILN">
          <xsl:value-of select="S[@n='Buyer']/L/F[@n='ILN']"/>
        </xsl:element>
        <xsl:element name="TaxID">
          <xsl:value-of select="S[@n='Buyer']/L/F[@n='TaxID']"/>
        </xsl:element>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='AccountNumber'])&gt;0">
          <xsl:element name="BankAccount">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='AccountNumber']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='Name'])&gt;0">
          <xsl:element name="Name">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='Name']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='StreetAndNumber'])&gt;0">
          <xsl:element name="Street">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='StreetAndNumber']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='CityName'])&gt;0">
          <xsl:element name="City">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='CityName']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='PostalCode'])&gt;0">
          <xsl:element name="PostalCode">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='PostalCode']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='Country'])&gt;0">
          <xsl:element name="Country">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='Country']"/>
          </xsl:element>
        </xsl:if>
      </xsl:element>

      <xsl:if  test="string-length(S[@n='Payer']/L/F[@n='ILN'])&gt;0">
        <xsl:element name="PayerParty">
          <xsl:element name="ILN">
            <xsl:value-of select="S[@n='Payer']/L/F[@n='ILN']"/>
          </xsl:element>
          <xsl:element name="TaxID">
            <xsl:value-of select="S[@n='Payer']/L/F[@n='TaxID']"/>
          </xsl:element>
          <xsl:if test="string-length(S[@n='Payer']/L/F[@n='AccountNumber'])&gt;0">
            <xsl:element name="BankAccount">
              <xsl:value-of select="S[@n='Payer']/L/F[@n='AccountNumber']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Payer']/L/F[@n='Name'])&gt;0">
            <xsl:element name="Name">
              <xsl:value-of select="S[@n='Payer']/L/F[@n='Name']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Payer']/L/F[@n='StreetAndNumber'])&gt;0">
            <xsl:element name="Street">
              <xsl:value-of select="S[@n='Payer']/L/F[@n='StreetAndNumber']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Payer']/L/F[@n='PostalCode'])&gt;0">
            <xsl:element name="PostalCode">
              <xsl:value-of select="S[@n='Payer']/L/F[@n='PostalCode']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Payer']/L/F[@n='CityName'])&gt;0">
            <xsl:element name="City">
              <xsl:value-of select="S[@n='Payer']/L/F[@n='CityName']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Payer']/L/F[@n='Country'])&gt;0">
            <xsl:element name="Country">
              <xsl:value-of select="S[@n='Payer']/L/F[@n='Country']"/>
            </xsl:element>
          </xsl:if>
        </xsl:element>
      </xsl:if>

      <xsl:if  test="string-length(S[@n='Invoicee']/L/F[@n='ILN'])&gt;0">
        <xsl:element name="InvoiceeParty">
          <xsl:element name="ILN">
            <xsl:value-of select="S[@n='Invoicee']/L/F[@n='ILN']"/>
          </xsl:element>
          <xsl:element name="TaxID">
            <xsl:value-of select="S[@n='Invoicee']/L/F[@n='TaxID']"/>
          </xsl:element>
          <xsl:if test="string-length(S[@n='Invoicee']/L/F[@n='AccountNumber'])&gt;0">
            <xsl:element name="BankAccount">
              <xsl:value-of select="S[@n='Invoicee']/L/F[@n='AccountNumber']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Invoicee']/L/F[@n='Name'])&gt;0">
            <xsl:element name="Name">
              <xsl:value-of select="S[@n='Invoicee']/L/F[@n='Name']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Invoicee']/L/F[@n='StreetAndNumber'])&gt;0">
            <xsl:element name="Street">
              <xsl:value-of select="S[@n='Invoicee']/L/F[@n='StreetAndNumber']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Invoicee']/L/F[@n='PostalCode'])&gt;0">
            <xsl:element name="PostalCode">
              <xsl:value-of select="S[@n='Invoicee']/L/F[@n='PostalCode']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Invoicee']/L/F[@n='CityName'])&gt;0">
            <xsl:element name="City">
              <xsl:value-of select="S[@n='Invoicee']/L/F[@n='CityName']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(S[@n='Invoicee']/L/F[@n='Country'])&gt;0">
            <xsl:element name="Country">
              <xsl:value-of select="S[@n='Invoicee']/L/F[@n='Country']"/>
            </xsl:element>
          </xsl:if>
        </xsl:element>
      </xsl:if>

      <xsl:element name="ShipToParty">
        <xsl:element name="ILN">
          <xsl:value-of select="S[@n='Delivery']/L/F[@n='DeliveryLocationNumber']"/>
        </xsl:element>
        <xsl:if test="string-length(S[@n='Delivery']/L/F[@n='Name'])&gt;0">
          <xsl:element name="Name">
            <xsl:value-of select="S[@n='Delivery']/L/F[@n='Name']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Delivery']/L/F[@n='StreetAndNumber'])&gt;0">
          <xsl:element name="Street">
            <xsl:value-of select="S[@n='Delivery']/L/F[@n='StreetAndNumber']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Delivery']/L/F[@n='PostalCode'])&gt;0">
          <xsl:element name="PostalCode">
            <xsl:value-of select="S[@n='Delivery']/L/F[@n='PostalCode']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Delivery']/L/F[@n='CityName'])&gt;0">
          <xsl:element name="City">
            <xsl:value-of select="S[@n='Delivery']/L/F[@n='CityName']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Buyer']/L/F[@n='Country'])&gt;0">
          <xsl:element name="Country">
            <xsl:value-of select="S[@n='Buyer']/L/F[@n='Country']"/>
          </xsl:element>
        </xsl:if>
      </xsl:element>

      <xsl:element name="SellerParty">
        <xsl:element name="ILN">
          <xsl:value-of select="S[@n='Seller']/L/F[@n='ILN']"/>
        </xsl:element>
        <xsl:element name="TaxID">
          <xsl:value-of select="S[@n='Seller']/L/F[@n='TaxID']"/>
        </xsl:element>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='AccountNumber'])&gt;0">
          <xsl:element name="BankAccount">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='AccountNumber']"/>
          </xsl:element>
        </xsl:if>
        <xsl:element name="BuyerSellerID">
          <xsl:value-of select="S[@n='Seller']/L/F[@n='CodeByBuyer']"/>
        </xsl:element>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='Name'])&gt;0">
          <xsl:element name="Name">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='Name']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='StreetAndNumber'])&gt;0">
          <xsl:element name="Street">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='StreetAndNumber']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='PostalCode'])&gt;0">
          <xsl:element name="PostalCode">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='PostalCode']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='CityName'])&gt;0">
          <xsl:element name="City">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='CityName']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='Country'])&gt;0">
          <xsl:element name="Country">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='Country']"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test="string-length(S[@n='Seller']/L/F[@n='CourtAndCapitalInformation'])&gt;0">
          <xsl:element name="RegisterInfo">
            <xsl:value-of select="S[@n='Seller']/L/F[@n='CourtAndCapitalInformation']"/>
          </xsl:element>
        </xsl:if>
      </xsl:element>

    </xsl:element>
  </xsl:template>

  <xsl:template name="header" match="Data" mode="Header">
    <xsl:element name="InvoiceHeader">
      <xsl:element name="InvoiceNumber">
        <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoiceNumber']"/>
      </xsl:element>
      <xsl:element name="Date">
        <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoiceDate']"/>
      </xsl:element>
      <xsl:element name="InvoiceIssueDate">
        <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='SalesDate']"/>
      </xsl:element>
      <xsl:element name="InvoiceDueDate">
        <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoicePaymentDueDate']"/>
      </xsl:element>
      <xsl:if test="string-length(S[@n='Invoice-Header']/L/F[@n='InvoiceDuplicateDate'])&gt;0">
        <xsl:element name="InvoiceDuplicateDate">
          <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoiceDuplicateDate']"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test="string-length(S[@n='Invoice-Header']/L/F[@n='InvoicePaymentTerms'])&gt;0">
        <xsl:element name="PaymentTerms">
          <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoicePaymentTerms']"/>
        </xsl:element>
        <xsl:element name="PaymentTermsReferenceDate">I</xsl:element>
      </xsl:if>
      <xsl:element name="PaymentMethod">
        <xsl:element name="Code">P</xsl:element>
        <xsl:element name="Description">Przelew</xsl:element>
      </xsl:element>
      <xsl:element name="InvoiceCurrencyCoded">
        <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='InvoiceCurrency']"/>
      </xsl:element>
      <xsl:element name="InvoicePurposeCoded">
        <xsl:if test="S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='O' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='D'">O</xsl:if>
        <xsl:if test="S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='C' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='R'">C</xsl:if>
      </xsl:element>
      <xsl:element name="DocumentRole">
        <xsl:if test="S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='O' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='C'">O</xsl:if>
        <xsl:if test="S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='D' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='R'">D</xsl:if>
      </xsl:element>
      <xsl:element name="Comment">
        <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='Remarks']"/>
      </xsl:element>
      <xsl:if test="string-length(S[@n='Reference']/L/F[@n='InvoiceReferenceNumber'])&gt;0">
        <xsl:element name="RefInvoiceDate">
          <xsl:value-of select="S[@n='Reference']/L/F[@n='InvoiceReferenceDate']"/>
        </xsl:element>
        <xsl:element name="RefInvoiceNumber">
          <xsl:value-of select="S[@n='Reference']/L/F[@n='InvoiceReferenceNumber']"/>
        </xsl:element>
        <xsl:element name="ReasonForCorrection">
          <xsl:value-of select="S[@n='Invoice-Header']/L/F[@n='CorrectionReason']"/>
        </xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <xsl:template name="Lines" match="Data" mode="Lines" >
    <xsl:element name="InvoiceDetail">
      <xsl:for-each select="S[@n='Invoice-Lines']/L">
        <xsl:element name="Item">
          <xsl:element name="ItemNum">
            <xsl:value-of select="F[@n='LineNumber']"/>
          </xsl:element>
          <xsl:element name="EAN">
            <xsl:value-of select="F[@n='EAN']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='BuyerItemCode'])&gt;0">
            <xsl:element name="BuyerItemID">
              <xsl:value-of select="F[@n='BuyerItemCode']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(F[@n='SupplierItemCode'])&gt;0">
            <xsl:element name="SellerItemID">
              <xsl:value-of select="F[@n='SupplierItemCode']"/>
            </xsl:element>
          </xsl:if>
          <xsl:element name="PacketContentQuantity">1</xsl:element>
          <!--<xsl:value-of select="F[@n='InvoiceUnitPackSize']-F[@n='InvoiceUnitPackSize']+1"/>-->
          <xsl:element name="PKWiU">
            <xsl:value-of select="F[@n='ReferenceNumber']"/>
          </xsl:element>
          <xsl:element name="QuantityValue">
            <xsl:value-of select="F[@n='InvoiceQuantity']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='PreviousInvoiceQuantity'])&gt;0">
            <xsl:element name="QuantityValueWas">
              <xsl:value-of select="F[@n='PreviousInvoiceQuantity']"/>
            </xsl:element>
            <xsl:if test="string-length(F[@n='CorrectionInvoiceQuantity'])&gt;0">
              <xsl:element name="QuantityValueDiff">
                <xsl:value-of select="F[@n='CorrectionInvoiceQuantity']"/>
              </xsl:element>
            </xsl:if>
          </xsl:if>
          <xsl:element name="TaxCategoryCoded">
            <xsl:value-of select="F[@n='TaxCategoryCode']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='PreviousTaxCategoryCode'])&gt;0">
            <xsl:element name="TaxCategoryCodedWas">
              <xsl:value-of select="F[@n='PreviousTaxCategoryCode']"/>
            </xsl:element>
          </xsl:if>
          <xsl:element name="TaxPercent">
            <xsl:value-of select="F[@n='TaxRate']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='PreviousTaxRate'])&gt;0">
            <xsl:element name="TaxPercentWas">
              <xsl:value-of select="F[@n='PreviousTaxRate']"/>
            </xsl:element>
          </xsl:if>
          <xsl:element name="TaxAmount">
            <xsl:value-of select="F[@n='TaxAmount']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='PreviousTaxAmount'])&gt;0">
            <xsl:element name="TaxAmountWas">
              <xsl:value-of select="F[@n='PreviousTaxAmount']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(F[@n='CorrectionTaxAmount'])&gt;0">
            <xsl:element name="TaxAmountDiff">
              <xsl:value-of select="F[@n='CorrectionTaxAmount']"/>
            </xsl:element>
          </xsl:if>
          <xsl:element name="MonetaryNetValue">
            <xsl:value-of select="F[@n='NetAmount']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='PreviousNetAmount'])&gt;0">
            <xsl:element name="MonetaryNetValueWas">
              <xsl:value-of select="F[@n='PreviousNetAmount']"/>
            </xsl:element>
          </xsl:if>
          <xsl:if test="string-length(F[@n='CorrectionNetAmount'])&gt;0">
            <xsl:element name="MonetaryNetValueDiff">
              <xsl:value-of select="F[@n='CorrectionNetAmount']"/>
            </xsl:element>
          </xsl:if>
          <xsl:element name="UnitOfMeasure">
            <xsl:value-of select="F[@n='UnitOfMeasure']"/>
          </xsl:element>
          <xsl:element name="UnitPriceValue">
            <xsl:value-of select="F[@n='InvoiceUnitNetPrice']"/>
          </xsl:element>
          <xsl:if test="string-length(F[@n='PreviousInvoiceUnitNetPrice'])&gt;0">
            <xsl:element name="UnitPriceValueWas">
              <xsl:value-of select="F[@n='PreviousInvoiceUnitNetPrice']"/>
            </xsl:element>
            <xsl:element name="UnitPriceValueDiff">
              <xsl:value-of select="F[@n='CorrectionInvoiceUnitNetPrice']"/>
            </xsl:element>
          </xsl:if>
          <xsl:element name="Name">
            <xsl:value-of select="F[@n='ItemDescription']"/>
          </xsl:element>
          <xsl:element name="ProductIdentifierExt">
            <xsl:value-of select="F[@n='ItemType']"/>
          </xsl:element>
          <xsl:element name="RefInvoice">
            <xsl:element name="RefInvoiceDate">
              <xsl:value-of select="F[@n='InvoiceReferenceDate']"/>
            </xsl:element>
            <xsl:element name="RefInvoiceNumber">
              <xsl:value-of select="F[@n='InvoiceReferenceNumber']"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="DeliveryDetail">
            <xsl:element name="ILN">
              <xsl:value-of select="F[@n='DeliveryLocationNumber']"/>
            </xsl:element>
            <xsl:element name="DeliveryDate">
              <xsl:value-of select="concat(F[@n='DeliveryDate'],'T12:00:00')"/>
            </xsl:element>
            <xsl:element name="DeliveryDocumentNumber">
              <xsl:value-of select="F[@n='DespatchNumber']"/>
            </xsl:element>
            <xsl:element name="DeliveryDocumentDate">
              <xsl:value-of select="F[@n='DespatchDate']"/>
            </xsl:element>
            <xsl:element name="PartyName">
              <xsl:value-of select="F[@n='Name']"/>
            </xsl:element>
            <xsl:element name="Street">
              <xsl:value-of select="F[@n='StreetAndNumber']"/>
            </xsl:element>
            <xsl:element name="CityName">
              <xsl:value-of select="F[@n='CityName']"/>
            </xsl:element>
            <xsl:element name="PostCode">
              <xsl:value-of select="F[@n='PostalCode']"/>
            </xsl:element>
            <xsl:element name="Country">
              <xsl:value-of select="F[@n='Country']"/>
            </xsl:element>
          </xsl:element>


          <!--<xsl:apply-templates select="*[@n!='ReferenceType' and @n!='ReferenceNumber']"/>
            <xsl:element name="TaxReference" >
              <xsl:element name="ReferenceType">
                <xsl:value-of select="F[@n='ReferenceType']"/>
              </xsl:element>
              <xsl:element name="ReferenceNumber">
                <xsl:value-of select="F[@n='ReferenceNumber']"/>
              </xsl:element>
            </xsl:element>
       </xsl:element>   -->
        </xsl:element>
      </xsl:for-each>

    </xsl:element>
  </xsl:template>

  <xsl:template name="footer" match="Data" mode="Footer">
    <xsl:element name="InvoiceSummary">
      <xsl:element name="NumberOfLines">
        <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='TotalLines']"/>
      </xsl:element>
      <xsl:element name="AmountInWords">
        <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='GrossAmountInWords']"/>
      </xsl:element>
      <xsl:element name="NetValue">
        <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='TotalNetAmount']"/>
      </xsl:element>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalNetAmount'])&gt;0">
        <xsl:element name="NetValueWas">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='PreviousTotalNetAmount']"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalNetAmount'])&gt;0">
        <xsl:element name="NetValueDiff">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalNetAmount']"/>
        </xsl:element>
      </xsl:if>
      <xsl:element name="TaxValue">
        <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='TotalTaxAmount']"/>
      </xsl:element>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxAmount'])&gt;0">
        <xsl:element name="TaxValueWas">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxAmount']"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxAmount'])&gt;0">
        <xsl:element name="TaxValueDiff">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxAmount']"/>
        </xsl:element>
      </xsl:if>
      <xsl:element name="TaxableValue">
        <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='TotalTaxableBasis']"/>
      </xsl:element>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxableBasis'])&gt;0">
        <xsl:element name="TaxableValueWas">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxableBasis']"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxableBasis'])&gt;0">
        <xsl:element name="TaxableValueDiff">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxableBasis']"/>
        </xsl:element>
      </xsl:if>
      <xsl:element name="GrossValue">
        <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='TotalGrossAmount']"/>
      </xsl:element>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalGrossAmount'])&gt;0">
        <xsl:element name="GrossValueWas">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='PreviousTotalGrossAmount']"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test="string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalGrossAmount'])&gt;0">
        <xsl:element name="GrossValueDiff">
          <xsl:value-of select="S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalGrossAmount']"/>
        </xsl:element>
      </xsl:if>
      <xsl:element name="TaxSummary">
        <xsl:for-each select="S[@n='Tax-Summary']/L">
          <xsl:element name="Tax">
            <xsl:element name="TaxCategoryCoded">
              <xsl:value-of select="F[@n='TaxCategoryCode']"/>
            </xsl:element>
            <xsl:if test="string-length(F[@n='PreviousTaxCategoryCode'])&gt;0">
              <xsl:element name="TaxCategoryCodedWas">
                <xsl:value-of select="F[@n='PreviousTaxCategoryCode']"/>
              </xsl:element>
            </xsl:if>
            <xsl:element name="TaxPercent">
              <xsl:value-of select="F[@n='TaxRate']"/>
            </xsl:element>
            <xsl:if test="string-length(F[@n='PreviousTaxRate'])&gt;0">
              <xsl:element name="TaxPercentWas">
                <xsl:value-of select="F[@n='PreviousTaxRate']"/>
              </xsl:element>
            </xsl:if>
            <xsl:element name="TaxNettoAmount">
              <xsl:value-of select="F[@n='TaxableAmount']"/>
            </xsl:element>
            <xsl:if test="string-length(F[@n='PreviousTaxableAmount'])&gt;0">
              <xsl:element name="TaxNettoAmountWas">
                <xsl:value-of select="F[@n='PreviousTaxableAmount']"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test="string-length(F[@n='CorrectionTaxableAmount'])&gt;0">
              <xsl:element name="TaxNettoAmountDiff">
                <xsl:value-of select="F[@n='CorrectionTaxableAmount']"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test="string-length(F[@n='TaxableBasis'])&gt;0">
              <xsl:element name="TaxableAmount">
                <xsl:value-of select="F[@n='TaxableBasis']"/>
              </xsl:element>
            </xsl:if>
            <xsl:element name="TaxAmount">
              <xsl:value-of select="F[@n='TaxAmount']"/>
            </xsl:element>
            <xsl:if test="string-length(F[@n='PreviousTaxAmount'])&gt;0">
              <xsl:element name="TaxAmountWas">
                <xsl:value-of select="F[@n='PreviousTaxAmount']"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test="string-length(F[@n='CorrectionTaxAmount'])&gt;0">
              <xsl:element name="TaxAmountDiff">
                <xsl:value-of select="F[@n='CorrectionTaxAmount']"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test="string-length(F[@n='GrossAmount'])&gt;0">
              <xsl:element name="TaxGrossAmount">
                <xsl:value-of select="F[@n='GrossAmount']"/>
              </xsl:element>
            </xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>

    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
