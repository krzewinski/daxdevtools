﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text;

public partial class UserDefinedFunctions
{
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString DecodeBase64(string input)
    {
        if (input == null) return "";
        byte[] data = Convert.FromBase64String(input);
        
        return new SqlString(Encoding.UTF8.GetString(data));
    }

    
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString DecodeBase64ByCodepage(string input, int codepage)
    {
        if (input == null) return "";
        byte[] data = Convert.FromBase64String(input);

        return new SqlString(Encoding.GetEncoding(codepage).GetString(data));
    }
};

