﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
namespace SpedCustDaxLib
{
    public class PackagesExport
    {
        public static string separator = ";";
        public static string Connectstring = "Data Source=daxyoda;Initial Catalog=APROD;Integrated Security=true;";
        public static string Cmd = @"SELECT *
FROM OPENROWSET(
    'Microsoft.ACE.OLEDB.12.0',
    'Excel 8.0;HDR=YES;Database=c:\1\Stokrotka_standy.xlsx',
    'select * from [stokrotka_standy1$]')";

        public static List<Package> packages = new List<Package>();
        public static void Run()
        {
            using (SqlConnection conn = new SqlConnection(Connectstring))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(Cmd, conn);
                SqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    packages.Add(new Package(rd));
                }
                StringBuilder sb = new StringBuilder();
                foreach (var pos in packages)
                {
                    pos.ToString(sb);
                }
                FileInfo fi = new FileInfo(@"c:\1\stokrotka.csv");
                if (fi.Exists) fi.Delete();
                
                

                using (StreamWriter sw = new StreamWriter(File.Open(fi.FullName, FileMode.Create), Encoding.Unicode))
                {
                    sw.Write(sb.ToString());
                }
                


            }
        }


    }
    public class Package
    {
        MainData main { get; set; }
        AssortmentData assort { get; set; }
        public Package(SqlDataReader rd)
        {

            main = new MainData() { Nadawca = new Nadawca(), Dostawa = new Dostawa(rd), Okna = new Okna(), Platnik = new Platnik(), Produkt = new Produkt() };
            assort = new AssortmentData();

        }
        public void ToString(StringBuilder sb)
        {
            

            this.main.ToString(sb);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(PackagesExport.separator);
            sb.Append(Environment.NewLine);
            this.assort.ToString(sb);
            sb.Append(Environment.NewLine);
        }

    }
    class MainData
    {
        public string Format { get { return "SC5_01"; } }
        public Nadawca Nadawca { get; set; }
        public Dostawa Dostawa { get; set; }
        public Platnik Platnik { get; set; }
        public Produkt Produkt { get; set; }
        public Okna Okna { get; set; }


        public void ToString(StringBuilder sb)
        {
            sb.Append(this.Format);
            sb.Append(PackagesExport.separator);
            Nadawca.ToString(sb);
            Dostawa.ToString(sb);
            Platnik.ToString(sb);
            Produkt.ToString(sb);
            Okna.ToString(sb);


        }
    }

    internal class Nadawca
    {
        public string Zleceniodawca { get { return "5356974"; } }
        public string Miejsce_Podjecia { get { return ""; } }
        public string Akronim { get { return ""; } }

        public string Nazwa1 { get { return ""; } }
        public string Nazwa2 { get { return ""; } }
        public string kod_pocztowy { get { return ""; } }
        public string Miasto { get { return ""; } }
        public string Ulica { get { return ""; } }
        public string NIP { get { return ""; } }
        public string Osoba_kontaktowa { get { return "MICHAŁ STAŃCZYK"; } }
        public string Telefon { get { return ""; } }
        public string Email { get { return ""; } }

        public void ToString(StringBuilder sb)
        {
            sb.Append(this.Zleceniodawca);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Miejsce_Podjecia);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Akronim);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Nazwa1);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Nazwa2);
            sb.Append(PackagesExport.separator);
            sb.Append(this.kod_pocztowy);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Miasto);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Ulica);
            sb.Append(PackagesExport.separator);
            sb.Append(this.NIP);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Osoba_kontaktowa);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Telefon);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Email);
            sb.Append(PackagesExport.separator);

        }
    }

    internal class Dostawa
    {
        public Dostawa()
        { }
        public Dostawa(SqlDataReader rd)
        {
            this.Akronim = rd["Akronim"].ToString();
            this.Nazwa1 = rd["Nazwa1"].ToString();
            this.Nazwa2 = rd["Nazwa2"].ToString();
            this.kod_pocztowy = rd["Kod"].ToString();
            this.Miasto = rd["Miejscowość"].ToString();
            this.Ulica = rd["Ulica"].ToString();
            //this.NIP = "nip";
            //this.Osoba_kontaktowa = "osoba";
            //this.Telefon = "tel";
            //this.Email = "email";

        }
        public string Akronim { get; set; }

        public string Nazwa1 { get; set; }
        public string Nazwa2 { get; set; }
        public string kod_pocztowy { get; set; }
        public string Miasto { get; set; }
        public string Ulica { get; set; }
        public string NIP { get; set; }
        public string Osoba_kontaktowa { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }

        public void ToString(StringBuilder sb)
        {



            sb.Append(this.Akronim);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Nazwa1);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Nazwa2);
            sb.Append(PackagesExport.separator);
            sb.Append(this.kod_pocztowy);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Miasto);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Ulica);
            sb.Append(PackagesExport.separator);
            sb.Append(this.NIP);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Osoba_kontaktowa);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Telefon);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Email);
            sb.Append(PackagesExport.separator);


        }
    }
    internal class Platnik
    {
        public string Rodzaj { get { return ""; } }
        public string NrDBShenker { get { return ""; } }
        public void ToString(StringBuilder sb)
        {



            sb.Append(this.Rodzaj);
            sb.Append(PackagesExport.separator);
            sb.Append(this.NrDBShenker);
            sb.Append(PackagesExport.separator);
        }
    }

    internal class Produkt
    {
        public string Kod { get { return "6"; } }

        public void ToString(StringBuilder sb)
        {



            sb.Append(this.Kod);
            sb.Append(PackagesExport.separator);

        }
    }


    internal class Okna
    {
        private DateTime _dataPodjeciaOd = new DateTime(2019, 4, 19);

        private DateTime _dataPodjeciaDo = new DateTime(2019, 4, 19);

        private DateTime _dataDostawyOd = new DateTime(2019, 4, 23);




        private DateTime _dataDostawyDo= new DateTime(2019, 4, 23);



        public DateTime DataPodjeciaDo
        {
            get { return _dataPodjeciaDo; }
            set { _dataPodjeciaDo = value; }
        }
        public DateTime DataPodjeciaOd
        {
            get { return _dataPodjeciaOd; }
            set { _dataPodjeciaOd = value; }
        }

        public DateTime DataDostawyOd
        {
            get { return _dataDostawyOd; }
            set { _dataDostawyOd = value; }
        }
        public DateTime DataDostawyDo
        {
            get { return _dataDostawyDo; }
            set { _dataDostawyDo = value; }
        }
        public void ToString(StringBuilder sb)
        {



            sb.Append(this.DataPodjeciaOd.ToString("yyyyMMdd"));
            sb.Append(PackagesExport.separator);
            sb.Append("");
            sb.Append(PackagesExport.separator);
            sb.Append("");
            sb.Append(PackagesExport.separator);
            sb.Append(this.DataDostawyDo.ToString("yyyyMMdd"));
            

        }
    }

    internal class AssortmentData
    {
        private string _nazwa = "STAND DAX SUN 2019 STOKROTKA 310000837";

       
        private string _kodJednoskiLogistycznej = "PC";
        private string _zabazpieczenie = "taśma";
        private double _ilosc = 1;
        private double _wagaJedn = 4.5;
        private double _wagaTotal = 4.5;
        private double _szerokosc = 0.15;
        private double _wysokosc = 1.2;
        private double _dlugosc = 0.6;
        public string Nazwa
        {
            get { return _nazwa; }
            set { _nazwa = value; }
        }
        public double Kubatura
        {
            get { return _szerokosc*_wysokosc*_dlugosc; }
            set {  }
        }

        public double Szerokosc
        {
            get { return _szerokosc; }
            set { _szerokosc = value; }
        }

       

        public double Dlugosc
        {
            get { return _dlugosc; }
            set { _dlugosc = value; }
        }

        

        public double Wysokosc
        {
            get { return _wysokosc; }
            set { _wysokosc = value; }
        }


        public double WagaTotal
        {
            get { return _wagaTotal; }
            set { _wagaTotal = value; }
        }

        public double WagaJedn
        {
            get { return _wagaJedn; }
            set { _wagaJedn = value; }
        }





        
        public string KodJednoskiLogistycznej
        {
            get { return _kodJednoskiLogistycznej; }
            set { _kodJednoskiLogistycznej = value; }
        }
        public string Zabazpieczenie
        {
            get { return _zabazpieczenie; }
            set { _zabazpieczenie = value; }
        }
        public double Ilosc
        {
            get { return _ilosc; }
            set { _ilosc = value; }
        }
        public void ToString(StringBuilder sb)
        {


            sb.Append("ASSORTMENTDATA");
            sb.Append(PackagesExport.separator);
            sb.Append(this.Nazwa);
            sb.Append(PackagesExport.separator);
            sb.Append(this.KodJednoskiLogistycznej);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Zabazpieczenie);
            sb.Append(PackagesExport.separator);
            sb.Append(this.Ilosc.ToString());
            sb.Append(PackagesExport.separator);
            sb.Append(this.WagaJedn.ToString());
            sb.Append(PackagesExport.separator);
            sb.Append(this.WagaTotal.ToString());
            sb.Append(PackagesExport.separator);
            sb.Append(this.Szerokosc.ToString());
            sb.Append(PackagesExport.separator);
            sb.Append(this.Dlugosc.ToString());
            sb.Append(PackagesExport.separator);
            sb.Append(this.Wysokosc.ToString());
            sb.Append(PackagesExport.separator);
            sb.Append(this.Kubatura.ToString());
            sb.Append(PackagesExport.separator);
            



        }
    }
}
