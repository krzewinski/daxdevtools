using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text.RegularExpressions;

public partial class UserDefinedFunctions
{
    const string regexStr = @"[\w-\.]+@(?<Domain>[\w-]+\.*[\w-]+\.+[\w-]{2,4})";
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString EmailFind(string input)
    {

        Regex r = new Regex(regexStr, RegexOptions.IgnoreCase);
        //Regex r = new Regex(@"[\w-]+", RegexOptions.IgnoreCase);

        if (r.IsMatch(input))
        {
            return (new SqlString(r.Match(input).Value));
        }
        // Put your code here
        return new SqlString(string.Empty);
    }
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString EmailDomainFind(string input)
    {

        Regex r = new Regex(regexStr, RegexOptions.IgnoreCase);
        //Regex r = new Regex(@"[\w-]+", RegexOptions.IgnoreCase);

        if (r.IsMatch(input))
        {
            return (new SqlString(r.Match(input).Groups["Domain"].Value));
        }
        // Put your code here
        return new SqlString(string.Empty);
    }
}
