﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace DataWarehouseToNav
{
    public class ReturnHandling
    {
        public decimal get(string docNo, string itemNo)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection conn =
                    new System.Data.SqlClient.SqlConnection
                        ("Data Source=daxsql;Initial Catalog=APROD;Integrated Security=True;MultipleActiveResultSets=True"))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(

    @"select avg(s.kosztWydaniaSprzedazy/s.iloscsprzedazy) Imt from hd_sprzedaz s where s.numerdokumentu = @docNo and kodAsortymentu =@itemNo", conn);
                    cmd.Parameters.Add(new SqlParameter("@docNo", docNo));
                    cmd.Parameters.Add(new SqlParameter("@itemNo", itemNo));

                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (rd.Read()) return (decimal?)rd[0] ?? 0;
                        else return 0;
                    }

                }



            }
            catch
            {
                return 0;
            }
        }
        }
    }

