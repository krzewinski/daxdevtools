﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

using System.IO;
using System.Reflection;
using System.Net;
using System.Net.Sockets;

using System.Text.RegularExpressions;
namespace DaxZPL_Lib
{

    public class ZPL
    {

        string Printer = "192.168.248.141:9100";
        public ZPL(string printerIp)
        {
            if (printerIp != "") Printer = printerIp;

        }

        string GetTextFileCntent(FileInfo fi)
        {
            string zpl = "";
            if (fi.Exists)
            {
                using (var reader = fi.OpenText())
                {
                    zpl = reader.ReadToEnd();


                }
            }
            return zpl;
        }



        public bool SendZplFile(FileInfo fi, int codepage)
        {
            if (!fi.Exists) return false;


            string zpl = GetTextFileCntent(fi);
            //zpl = ctxt.Parse(zpl);
            return SendZplFile(zpl, codepage);





        }

        void getResponse(Socket socket)
        {
            int size = 7000;
            byte[] buffer = new byte[size];

            socket.Receive(buffer);
            System.Diagnostics.Debug.Write(
                System.Text.Encoding.UTF8.GetString(buffer)
                );
        }
        public bool SendZplFile(string zpl, int codepage)
        {

            //zpl = ctxt.Parse(zpl);

            int port = 9100;
            string[] ipParts = this.Printer.Split(new char[] { ':' });
            this.Printer = ipParts[0];
            if (ipParts.Count() > 1) port = int.Parse(ipParts[1]);

            System.Net.Sockets.NetworkStream ns = null;
            System.Net.Sockets.Socket socket = null;
            System.Net.IPEndPoint printerIP = null;
            byte[] toSend;

            try
            {
                if (printerIP == null)
                {
                    //set the IP address
                    printerIP = new System.Net.IPEndPoint(IPAddress.Parse(this.Printer), port);
                }

                //Create a TCP socket
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //Connect to the printer based on the IP address
                socket.Connect(printerIP);
                //create a new network stream based on the socket connection
                ns = new NetworkStream(socket);

                //convert the zpl command to a byte array
                //toSend = System.Text.Encoding.GetEncoding(852).GetBytes(zpl);
                toSend = System.Text.Encoding.UTF8.GetBytes(zpl);
                //toSend = System.Text.Encoding.GetEncoding(codepage).GetBytes(zpl);
                //send the zpl byte array over the networkstream to the connected printer
                ns.Write(toSend, 0, toSend.Length);
                ns.Flush();
                //getResponse(socket);


            }
            catch (Exception ex)
            {
                //WriteToLog(ex.Message);
                System.Diagnostics.Debug.Fail(ex.Message);
                return false;

            }
            finally
            {
                //close the networkstream and then the socket
                if (ns != null)
                {
                    ns.Close();
                }

                if (socket != null)
                {
                    socket.Close();
                }

            }
            return true;

        }
    }
}
