﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Xml.Linq;
namespace XmlEncoderDecoder
{
    public partial class DaxEncoder : Form
    {
        public DaxEncoder()
        {
            InitializeComponent();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var unescaped = richTextBox1.Text;
                var strVal = "";
                using (StringReader sr = new StringReader(richTextBox1.Text))
                {
                    //using (XmlTextReader xread = new XmlTextReader(sr))
                    //{

                    //    if (!xread.Read()) return;
                    //    if (!xread.Read()) return;


                    //    EncoderDecoder enc = new EncoderDecoder(xread.ReadOuterXml());
                    //    richTextBox2.Text = enc.Encode();

                    //}

                    if (!string.IsNullOrEmpty(unescaped))
                    {

                        strVal = new XAttribute("n", unescaped).ToString().Replace("'", "&#39;");
                        
                    }
                    richTextBox2.Text = strVal;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.HandleException(ex, true);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                EncoderDecoder enc = new EncoderDecoder(richTextBox2.Text);
                richTextBox1.Text = enc.Decode();
            }
            catch (Exception ex)
            {
                ErrorHandler.HandleException(ex, true);
            }
        }
    }
}
