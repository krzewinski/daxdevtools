﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace XmlEncoderDecoder
{
    public class EncoderDecoder
    {
        string inputString = "";
        public EncoderDecoder(string inputString)
        {
            //this.inputString = inputString;
            this.inputString = System.Net.WebUtility.HtmlDecode(inputString);

        }

        public string Decode()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerXml = this.inputString;
            return node.InnerXml;
        }

        public string Encode()
        {

            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerText = this.inputString;
            return System.Net.WebUtility.HtmlEncode( node.InnerXml);
            //return node.InnerXml;
            
        }
    }
}
