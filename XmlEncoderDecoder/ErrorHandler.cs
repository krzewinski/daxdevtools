﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlEncoderDecoder
{
    internal class ErrorHandler
    {
        internal static void HandleException(Exception ex, bool showMessage)
        {
            if (showMessage)
            {
                StringBuilder sb = new StringBuilder();
                
                    sb.Append(string.Format("Wystąpił wyjątek:{0}\r\n", ex.Message));
                    sb.Append(string.Format("stackTrace:{0}\r\n", ex.StackTrace));
                    sb.Append(string.Format("Source:{0}\r\n", ex.Source));
                    System.Windows.Forms.MessageBox.Show(sb.ToString());
                
            }

        }
    }
}
