﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XmlEncoderDecoder
{
    public partial class BinDecoder : Form
    {
        public BinDecoder()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string binary;
            binary = richTextBox1.Text;
            
            string[] binLetter  = binary.Split(new char[] { ' ' });
            StringBuilder sb = new StringBuilder();

            foreach(var pos in binLetter)
            {
                long l = Convert.ToInt64(pos, 2);
                char letter = Convert.ToChar(l);
                sb.Append(letter);
            }

            MessageBox.Show(sb.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string tBeCoded = richTextBox2.Text;

            byte[] b =Encoding.UTF8.GetBytes(tBeCoded);
            this.richTextBox1.Text= string.Join(" ",b.Select (x=>Convert.ToString(x, 2).PadLeft(8,'0')));

        }
    }
}
