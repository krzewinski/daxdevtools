﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web;
using System.IO;
using System.Xml;
using Dax_LabelPrinter;

namespace Testy
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        [TestMethod]
        public void BinaryField()
        {
            var str = "";
            var bytes = Encoding.UTF8.GetBytes(str);
            var binStr = string.Join("", bytes.Select(b => Convert.ToString(b, 8)));
        }

        [TestMethod]
        public void decodeXml()
        {
            string xml = @"&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;&#xD;&#xA;&lt;xsl:stylesheet version=&quot;2.0&quot; xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;      xmlns:msxsl=&quot;urn:schemas-microsoft-com:xslt&quot;      exclude-result-prefixes=&quot;msxsl&quot;      xmlns:ms=&quot;urn:schemas-microsoft-com:xslt&quot;      xmlns:dt=&quot;urn:schemas-microsoft-com:datatypes&quot;&gt;&#xD;&#xA;  &lt;xsl:output method=&quot;xml&quot; encoding=&quot;UTF-8&quot; /&gt;&#xD;&#xA;  &lt;xsl:template match=&quot;/&quot;&gt;&#xD;&#xA;    &lt;xsl:element name=&quot;Invoice&quot;&gt;&#xD;&#xA;      &lt;xsl:apply-templates select=&quot;Evenlope/Data&quot; mode=&quot;Header&quot; /&gt;&#xD;&#xA;      &lt;xsl:apply-templates select=&quot;Evenlope/Data&quot; mode=&quot;Parties&quot; /&gt;&#xD;&#xA;      &lt;xsl:apply-templates select=&quot;Evenlope/Data&quot; mode=&quot;Lines&quot; /&gt;&#xD;&#xA;      &lt;xsl:apply-templates select=&quot;Evenlope/Data&quot; mode=&quot;Footer&quot; /&gt;&#xD;&#xA;    &lt;/xsl:element&gt;&#xD;&#xA;  &lt;/xsl:template&gt;&#xD;&#xA;  &lt;xsl:template name=&quot;InvoiceParties&quot; match=&quot;Data&quot; mode=&quot;Parties&quot; &gt;&#xD;&#xA;    &lt;xsl:element name=&quot;InvoiceParty&quot;&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;OrderParty&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;BuyerOrderNumber&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Order']/L/F[@n='BuyerOrderNumber']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;BuyerOrderDate&quot;&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Order']/L/F[@n='BuyerOrderDate'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Order']/L/F[@n='BuyerOrderDate']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;not (string-length(S[@n='Order']/L/F[@n='BuyerOrderDate'])&amp;gt;0)&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoiceDate']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Delivery']/L/F[@n='DeliveryLocationNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;DeliveryParty&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;DespatchAdviceNumber&quot;&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(S[@n='Delivery']/L/F[@n='DespatchNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Delivery']/L/F[@n='DespatchNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;not (string-length(S[@n='Delivery']/L/F[@n='DespatchNumber'])&amp;gt;0)&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoice-Lines']/L[@i='1']/F[@n='DespatchNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:choose&gt;&#xD;&#xA;            &lt;xsl:when test=&quot;string-length(S[@n='Delivery']/L/F[@n='DespatchDate'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;DespatchAdviceDate&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;concat(S[@n='Delivery']/L/F[@n='DespatchDate'],'T12:00:00')&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:when&gt;&#xD;&#xA;            &lt;xsl:when test=&quot;string-length(S[@n='Delivery']/L/F[@n='DeliveryDate'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;DespatchAdviceDate&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;concat(S[@n='Delivery']/L/F[@n='DeliveryDate'],'T12:00:00')&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:when&gt;&#xD;&#xA;            &lt;xsl:when test=&quot;string-length(S[@n='Invoice-Lines']/L[@i='1']/F[@n='DespatchDate'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;DespatchAdviceDate&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;concat(S[@n='Invoice-Lines']/L[@i='1']/F[@n='DespatchDate'],'T12:00:00')&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:when&gt;&#xD;&#xA;            &lt;xsl:when test=&quot;string-length(S[@n='Invoice-Lines']/L[@i='1']/F[@n='DeliveryDate'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;DespatchAdviceDate&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;concat(S[@n='Invoice-Lines']/L[@i='1']/F[@n='DeliveryDate'],'T12:00:00')&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:when&gt;&#xD;&#xA;            &lt;xsl:otherwise&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;DespatchAdviceDate&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;concat(S[@n='Invoice-Header']/L/F[@n='InvoiceDate'],'T12:00:00')&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:otherwise&gt;&#xD;&#xA;          &lt;/xsl:choose&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;BuyerParty&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;ILN&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='ILN']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;TaxID&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='TaxID']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Buyer']/L/F[@n='AccountNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;BankAccount&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='AccountNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Buyer']/L/F[@n='Name'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Name&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='Name']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Buyer']/L/F[@n='StreetAndNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Street&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='StreetAndNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;PostalCode&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='PostalCode']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;City&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='CityName']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;Country&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='Country']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if  test=&quot;string-length(S[@n='Payer']/L/F[@n='ILN'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;PayerParty&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;ILN&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='ILN']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;TaxID&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='TaxID']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Payer']/L/F[@n='AccountNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;BankAccount&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='AccountNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Payer']/L/F[@n='Name'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;Name&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='Name']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Payer']/L/F[@n='StreetAndNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;Street&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='StreetAndNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Payer']/L/F[@n='PostalCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;PostalCode&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='PostalCode']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Payer']/L/F[@n='CityName'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;City&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='CityName']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Payer']/L/F[@n='Country'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;Country&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Payer']/L/F[@n='Country']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:if  test=&quot;string-length(S[@n='Invoicee']/L/F[@n='ILN'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;InvoiceeParty&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;ILN&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='ILN']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;TaxID&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='TaxID']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Invoicee']/L/F[@n='AccountNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;BankAccount&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='AccountNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Invoicee']/L/F[@n='Name'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;Name&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='Name']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Invoicee']/L/F[@n='StreetAndNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;Street&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='StreetAndNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Invoicee']/L/F[@n='PostalCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;PostalCode&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='PostalCode']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Invoicee']/L/F[@n='CityName'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;City&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='CityName']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(S[@n='Invoicee']/L/F[@n='Country'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;Country&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoicee']/L/F[@n='Country']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;ShipToParty&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;ILN&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Delivery']/L/F[@n='DeliveryLocationNumber']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Delivery']/L/F[@n='Name'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Name&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Delivery']/L/F[@n='Name']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Delivery']/L/F[@n='StreetAndNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Street&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Delivery']/L/F[@n='StreetAndNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Delivery']/L/F[@n='PostalCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;PostalCode&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Delivery']/L/F[@n='PostalCode']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Delivery']/L/F[@n='CityName'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;City&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Delivery']/L/F[@n='CityName']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Buyer']/L/F[@n='Country'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Country&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Buyer']/L/F[@n='Country']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;SellerParty&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;ILN&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='ILN']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;TaxID&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='TaxID']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='AccountNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;BankAccount&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='AccountNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;BuyerSellerID&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='CodeByBuyer']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='Name'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Name&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='Name']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='StreetAndNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Street&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='StreetAndNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='PostalCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;PostalCode&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='PostalCode']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='CityName'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;City&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='CityName']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='Country'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Country&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='Country']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;string-length(S[@n='Seller']/L/F[@n='CourtAndCapitalInformation'])&amp;gt;0&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;RegisterInfo&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;S[@n='Seller']/L/F[@n='CourtAndCapitalInformation']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:if&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;    &lt;/xsl:element&gt;&#xD;&#xA;  &lt;/xsl:template&gt;&#xD;&#xA;  &lt;xsl:template name=&quot;header&quot; match=&quot;Data&quot; mode=&quot;Header&quot;&gt;&#xD;&#xA;    &lt;xsl:element name=&quot;InvoiceHeader&quot;&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;InvoiceNumber&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoiceNumber']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;Date&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoiceDate']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;InvoiceIssueDate&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='SalesDate']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;InvoiceDueDate&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoicePaymentDueDate']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Header']/L/F[@n='InvoiceDuplicateDate'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;InvoiceDuplicateDate&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoiceDuplicateDate']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Header']/L/F[@n='InvoicePaymentTerms'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;PaymentTerms&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoicePaymentTerms']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;PaymentTermsReferenceDate&quot;&gt;I&lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;PaymentMethod&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;Code&quot;&gt;P&lt;/xsl:element&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;Description&quot;&gt;Przelew&lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;InvoiceCurrencyCoded&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoiceCurrency']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;InvoicePurposeCoded&quot;&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='O' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='D'&quot;&gt;O&lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='C' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='R'&quot;&gt;C&lt;/xsl:if&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;DocumentRole&quot;&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='O' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='C'&quot;&gt;O&lt;/xsl:if&gt;&#xD;&#xA;        &lt;xsl:if test=&quot;S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='D' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='R'&quot;&gt;D&lt;/xsl:if&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;Comment&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='Remarks']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &#xD;&#xA;      &lt;xsl:if test=&quot;S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='C' or S[@n='Invoice-Header']/L/F[@n='DocumentFunctionCode']='R'&quot;&gt;&#xD;&#xA;        &lt;xsl:choose&gt;&#xD;&#xA;          &lt;xsl:when test=&quot;string-length(S[@n='Reference']/L/F[@n='InvoiceReferenceNumber'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceDate&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Reference']/L/F[@n='InvoiceReferenceDate']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceNumber&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Reference']/L/F[@n='InvoiceReferenceNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:when&gt;&#xD;&#xA;          &lt;xsl:when test=&quot;substring(S[@n='Invoice-Header']/L/F[@n='InvoiceNumber'],1,3)='FKB'&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceDate&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoice-Header']/L/F[@n='InvoiceDate']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceNumber&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Order']/L/F[@n='BuyerOrderNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:when&gt;&#xD;&#xA;          &lt;xsl:otherwise&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceDate&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoice-Lines']/L[@i='1']/F[@n='InvoiceReferenceDate']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceNumber&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;S[@n='Invoice-Lines']/L[@i='1']/F[@n='InvoiceReferenceNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:otherwise&gt;&#xD;&#xA;        &lt;/xsl:choose&gt;        &#xD;&#xA;        &lt;xsl:element name=&quot;ReasonForCorrection&quot;&gt;&#xD;&#xA;          &lt;xsl:choose&gt;&#xD;&#xA;            &lt;xsl:when test=&quot;substring(S[@n='Invoice-Header']/L/F[@n='InvoiceNumber'],1,3)='FKB'&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;3&quot;/&gt;&#xD;&#xA;            &lt;/xsl:when&gt;&#xD;&#xA;            &lt;xsl:when test=&quot;S[@n='Order']/L/F[@n='BuyerOrderNumber']='99999999'&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;2&quot;/&gt;&#xD;&#xA;            &lt;/xsl:when&gt;&#xD;&#xA;            &lt;xsl:otherwise&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;1&quot;/&gt;              &#xD;&#xA;            &lt;/xsl:otherwise&gt;&#xD;&#xA;          &lt;/xsl:choose&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;    &lt;/xsl:element&gt;&#xD;&#xA;  &lt;/xsl:template&gt;&#xD;&#xA;  &lt;xsl:template name=&quot;Lines&quot; match=&quot;Data&quot; mode=&quot;Lines&quot; &gt;&#xD;&#xA;    &lt;xsl:element name=&quot;InvoiceDetail&quot;&gt;&#xD;&#xA;      &lt;xsl:for-each select=&quot;S[@n='Invoice-Lines']/L&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;Item&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;ItemNum&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='LineNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;EAN&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='EAN']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='BuyerItemCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;BuyerItemID&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='BuyerItemCode']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='SupplierItemCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;SellerItemID&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='SupplierItemCode']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;PacketContentQuantity&quot;&gt;1&lt;/xsl:element&gt;&#xD;&#xA;          &lt;!--&lt;xsl:value-of select=&quot;F[@n='InvoiceUnitPackSize']-F[@n='InvoiceUnitPackSize']+1&quot;/&gt;--&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;PKWiU&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='ReferenceNumber']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;QuantityValue&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='InvoiceQuantity']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='PreviousInvoiceQuantity'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;QuantityValueWas&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='PreviousInvoiceQuantity']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='CorrectionInvoiceQuantity'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;QuantityValueDiff&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='CorrectionInvoiceQuantity']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;TaxCategoryCoded&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='TaxCategoryCode']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxCategoryCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxCategoryCodedWas&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='PreviousTaxCategoryCode']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;TaxPercent&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='TaxRate']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxRate'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxPercentWas&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='PreviousTaxRate']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;TaxAmount&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='TaxAmount']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxAmountWas&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='PreviousTaxAmount']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='CorrectionTaxAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxAmountDiff&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='CorrectionTaxAmount']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;MonetaryNetValue&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='NetAmount']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='PreviousNetAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;MonetaryNetValueWas&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='PreviousNetAmount']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='CorrectionNetAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;MonetaryNetValueDiff&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='CorrectionNetAmount']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;UnitOfMeasure&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='UnitOfMeasure']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;UnitPriceValue&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='InvoiceUnitNetPrice']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:if test=&quot;string-length(F[@n='PreviousInvoiceUnitNetPrice'])&amp;gt;0&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;UnitPriceValueWas&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='PreviousInvoiceUnitNetPrice']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;UnitPriceValueDiff&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='CorrectionInvoiceUnitNetPrice']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:if&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Name&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='ItemDescription']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;ProductIdentifierExt&quot;&gt;&#xD;&#xA;            &lt;xsl:value-of select=&quot;F[@n='ItemType']&quot;/&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;RefInvoice&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceDate&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='InvoiceReferenceDate']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;RefInvoiceNumber&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='InvoiceReferenceNumber']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:for-each&gt;&#xD;&#xA;    &lt;/xsl:element&gt;&#xD;&#xA;  &lt;/xsl:template&gt;&#xD;&#xA;  &lt;xsl:template name=&quot;footer&quot; match=&quot;Data&quot; mode=&quot;Footer&quot;&gt;&#xD;&#xA;    &lt;xsl:element name=&quot;InvoiceSummary&quot;&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;NumberOfLines&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='TotalLines']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;AmountInWords&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='GrossAmountInWords']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;NetValue&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='TotalNetAmount']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalNetAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;NetValueWas&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='PreviousTotalNetAmount']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalNetAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;NetValueDiff&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalNetAmount']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;TaxValue&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='TotalTaxAmount']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;TaxValueWas&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxAmount']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;TaxValueDiff&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxAmount']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;TaxableValue&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='TotalTaxableBasis']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxableBasis'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;TaxableValueWas&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='PreviousTotalTaxableBasis']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxableBasis'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;TaxableValueDiff&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalTaxableBasis']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;GrossValue&quot;&gt;&#xD;&#xA;        &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='TotalGrossAmount']&quot;/&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='PreviousTotalGrossAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;GrossValueWas&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='PreviousTotalGrossAmount']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:if test=&quot;string-length(S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalGrossAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;        &lt;xsl:element name=&quot;GrossValueDiff&quot;&gt;&#xD;&#xA;          &lt;xsl:value-of select=&quot;S[@n='Invoice-Summary']/L/F[@n='CorrectionTotalGrossAmount']&quot;/&gt;&#xD;&#xA;        &lt;/xsl:element&gt;&#xD;&#xA;      &lt;/xsl:if&gt;&#xD;&#xA;      &lt;xsl:element name=&quot;TaxSummary&quot;&gt;&#xD;&#xA;        &lt;xsl:for-each select=&quot;S[@n='Tax-Summary']/L&quot;&gt;&#xD;&#xA;          &lt;xsl:element name=&quot;Tax&quot;&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxCategoryCoded&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='TaxCategoryCode']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxCategoryCode'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxCategoryCodedWas&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='PreviousTaxCategoryCode']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxPercent&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='TaxRate']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxRate'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxPercentWas&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='PreviousTaxRate']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxNettoAmount&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='TaxableAmount']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxableAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxNettoAmountWas&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='PreviousTaxableAmount']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='CorrectionTaxableAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxNettoAmountDiff&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='CorrectionTaxableAmount']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='TaxableBasis'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxableAmount&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='TaxableBasis']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:element name=&quot;TaxAmount&quot;&gt;&#xD;&#xA;              &lt;xsl:value-of select=&quot;F[@n='TaxAmount']&quot;/&gt;&#xD;&#xA;            &lt;/xsl:element&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='PreviousTaxAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxAmountWas&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='PreviousTaxAmount']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='CorrectionTaxAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxAmountDiff&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='CorrectionTaxAmount']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;            &lt;xsl:if test=&quot;string-length(F[@n='GrossAmount'])&amp;gt;0&quot;&gt;&#xD;&#xA;              &lt;xsl:element name=&quot;TaxGrossAmount&quot;&gt;&#xD;&#xA;                &lt;xsl:value-of select=&quot;F[@n='GrossAmount']&quot;/&gt;&#xD;&#xA;              &lt;/xsl:element&gt;&#xD;&#xA;            &lt;/xsl:if&gt;&#xD;&#xA;          &lt;/xsl:element&gt;&#xD;&#xA;        &lt;/xsl:for-each&gt;&#xD;&#xA;      &lt;/xsl:element&gt;&#xD;&#xA;    &lt;/xsl:element&gt;&#xD;&#xA;  &lt;/xsl:template&gt;&#xD;&#xA;&lt;/xsl:stylesheet&gt;&#xD;&#xA;";
            System.IO.StringReader sr = new System.IO.StringReader(xml);
            System.Xml.XmlTextReader xr = new System.Xml.XmlTextReader(sr);

            string result = HttpUtility.HtmlDecode(xml);
        }

        [TestMethod]
        public void TestMethod1()
        {
            DataWarehouseToNav.ReturnHandling rh = new DataWarehouseToNav.ReturnHandling();
            decimal a = rh.get("FV 19533/12/09", "010021033");

        }
        [TestMethod]
        public void downloadAprodFiles()
        {



            List<DocumentInfo> documentInfos = new List<DocumentInfo>();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Data Source=daxyoda;Initial Catalog=SIT_ERP;Integrated Security=True;MultipleActiveResultSets=True"))
            {
                conn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(
                    @"select a.kod, dp.id posID,u.login_ad from dbo.documents d
join dbo.documentPositions dp on dp.idDocument=d.id
join aprod.dbo.dokumentyZapasow dz on dz.idDokumentu = d.id
join aprod.dbo.asortyment a on a.id = dz.idZapasu
left outer join aprod.dbo.uzytkownicy u on u.id = d.iduser
where d.idDocType='fileDoc'",
                    conn))
                {
                    System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        documentInfos.Add(
                            new DocumentInfo() { kodAsort = (string)dr["kod"], posId = (int)dr["posID"], username = (string)dr["login_ad"] });
                    }

                }

            }
            //sciaganie pliku z bazy
            foreach (var docInfo in documentInfos)
            {
                docInfo.Download();
            }
            //Response.End();
            //return RedirectToAction("Documents", new { id = asortId });
        }
        [TestMethod]
        public void ReloadB2BMissingMessages()
        {
            List<EdiOrderInfo> list = new List<EdiOrderInfo>();
            #region wpisywanie listy do rpzeszukania
            //list.Add(new EdiOrderInfo() { BuyerIln = "5900001240007", BuyerOrderNumber = "27092365", OrderNumber = "42395123" });
            //list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4543820894" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592798" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592798" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592798" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592798" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592798" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592931" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592932" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592799" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592798" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592931" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592932" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592933" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592934" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592935" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592805" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592931" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592932" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592933" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592934" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592935" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592936" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592937" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592801" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592803" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592802" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592804" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592931" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592932" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592933" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592934" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592935" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592937" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592936" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592938" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592806" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592810" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592808" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592921" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592922" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592923" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592924" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592925" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592926" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592927" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592931" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592932" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592933" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592934" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592935" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592936" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592937" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592938" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592449" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592453" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592461" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592457" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592465" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592939" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592940" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547556865" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 299/01/03/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 298/01/03/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 297/01/03/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23098632" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547592941" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23115279" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7200003349" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5907468149942", BuyerOrderNumber = "065944/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1605055" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23100279" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000556713", BuyerOrderNumber = "C006811515" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23115279" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7200003349" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5907468149942", BuyerOrderNumber = "065944/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1605055" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 5/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 7/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 7/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584851" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 7/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 7/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584759" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584758" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 7/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584759" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584756" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584754" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584759" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584753" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584752" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584751" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584756" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584749" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584747" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584749" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584745" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584741" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584735" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584682" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547584674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547562938" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547562939" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547595783" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607258" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607259" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607260" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607284" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607285" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000889118", BuyerOrderNumber = "ZZ-56/04/23" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607563" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607571" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607582" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607583" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607584" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607585" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000556713", BuyerOrderNumber = "C006817030" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544402" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544406" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544403" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544406" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544404" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544489" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544491" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544437" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544485" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544491" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544437" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001544440" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23118596" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23118605" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "8201464711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025056" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23119875" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611603" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611636" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611637" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611638" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611638" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611639" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611640" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611638" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611639" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611642" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611638" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611639" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611642" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611639" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611638" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611646" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611647" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611642" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611647" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611648" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611638" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611639" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611646" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611647" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611648" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611649" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611642" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611647" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611648" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611649" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611647" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611646" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611648" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611649" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611642" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611643" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611644" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611647" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611649" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611672" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611673" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611672" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611673" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611675" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611676" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611677" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611678" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611679" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611672" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611673" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611675" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611676" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611677" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611678" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611679" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611680" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611681" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611682" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611683" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611684" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611672" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611673" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611675" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611676" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611677" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611678" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611679" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611680" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611681" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611682" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611683" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611684" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611686" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611672" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611673" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611674" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611675" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611676" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611677" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611678" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611679" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611680" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611681" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611682" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611683" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611684" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611686" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611672" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611675" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611677" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611676" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611678" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611679" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611680" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611681" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611682" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611683" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611684" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611686" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611697" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611699" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611680" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611681" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611682" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611683" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611684" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611686" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611697" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611699" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611712" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611656" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611686" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611697" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611699" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611712" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611717" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611718" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611719" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611722" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611723" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611697" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611699" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611712" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611717" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611718" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611719" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611722" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611723" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611727" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611697" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611699" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611712" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611717" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611718" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611719" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611722" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611723" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611727" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611730" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611731" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611732" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611733" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611735" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611683" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611684" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611685" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611686" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611687" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611697" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611698" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611699" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611712" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611717" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611718" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611719" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611722" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611723" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611727" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611730" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611731" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611732" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611733" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611735" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611736" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611737" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611738" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611739" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611740" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611741" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611742" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611743" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611670" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611671" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611680" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611681" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611713" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611712" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611716" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611717" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611718" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611719" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611722" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611723" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611727" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611730" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611731" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611732" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611733" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611735" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611736" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611737" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611738" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611739" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611740" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611741" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611742" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611743" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611744" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611745" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611747" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611746" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611748" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611751" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611700" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611701" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611702" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611703" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611704" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611705" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611706" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611717" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611718" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611719" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611721" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611722" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611723" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611727" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611730" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611731" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611732" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611733" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611735" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611736" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611737" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611738" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611739" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611740" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611741" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611689" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611690" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611691" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611742" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611743" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611744" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611709" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611710" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611745" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611746" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611747" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611748" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611751" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611752" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611753" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611754" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611755" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611756" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611757" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611758" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611759" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611724" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611725" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611726" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611727" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611728" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611729" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611730" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611731" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611732" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611733" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611734" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611735" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611736" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611737" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611738" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611739" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611740" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611741" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611742" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611743" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611744" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611745" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611746" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611747" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611692" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611693" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611694" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611748" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611751" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611695" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611696" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611752" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611753" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611754" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611755" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611756" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611757" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611758" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611759" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611760" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611761" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611762" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611763" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611764" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611765" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611766" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611767" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611768" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611769" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611770" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611771" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611772" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611773" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611774" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611775" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611776" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547611777" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001546227" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001546226" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014100312", BuyerOrderNumber = "00768887" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014102514", BuyerOrderNumber = "00485099" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184510", BuyerOrderNumber = "00544498" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184312", BuyerOrderNumber = "00544513" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184510", BuyerOrderNumber = "00544650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184312", BuyerOrderNumber = "00544688" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025602" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025169" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025609" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025466" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025587" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23122057" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610890" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610891" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610890" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610892" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610890" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610891" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1610893" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000025626" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624128" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624129" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624131" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624128" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624132" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624133" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624129" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624128" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624131" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624132" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624133" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624128" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624129" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624133" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624131" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624133" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624132" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624128" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624129" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624651" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624131" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624132" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624133" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624128" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624129" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624131" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624132" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624133" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624653" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624658" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624659" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624661" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625194" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625195" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625196" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624666" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625194" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625195" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625196" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625198" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625197" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624665" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625194" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625195" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625196" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625197" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625198" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625199" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625200" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625201" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624652" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624660" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624657" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625194" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625195" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625197" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625196" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625198" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625199" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625200" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625201" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625202" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625203" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625204" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624668" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624669" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624667" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625183" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625184" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625185" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625194" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625195" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625196" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625197" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625198" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625199" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625200" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625201" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625202" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625203" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625204" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625205" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625206" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625207" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625209" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625210" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625211" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625213" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625212" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624650" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624654" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624662" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624664" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547624663" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625186" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625187" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625188" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625189" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625190" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625191" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625192" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625193" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625194" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625195" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625196" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625197" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625198" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625199" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625200" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625201" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625203" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625202" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625206" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625204" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625205" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625207" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625208" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625214" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625214" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 88/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625214" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 88/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 86/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000836464", BuyerOrderNumber = "COMARCHZD 89/01/04/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625212" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607583" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625212" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607583" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625212" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607583" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909002071009", BuyerOrderNumber = "4547625212" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1607583" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1611784" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1611783" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1611785" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000556713", BuyerOrderNumber = "C006827552" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547640" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547641" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547640" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547642" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949110" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05503760" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949110" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05503760" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05503760" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949110" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547714" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949150" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949140" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547708" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949150" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05503760" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05949140" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05503750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547711" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547893" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001547892" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "1600002849" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001548004" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001548005" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23125995" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126079" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "1600002854" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2971" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2971" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2972" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2971" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2972" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2971" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900001420003", BuyerOrderNumber = "2023/MC99/2972" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5907468149942", BuyerOrderNumber = "071685/2023" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126779" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3203917750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3203917750" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3203917749" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3203917751" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001548105" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001548104" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001548105" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5901571700007", BuyerOrderNumber = "3001548104" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000026091" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000026091" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000026091" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05952490" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05507800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05952490" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05507800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184312", BuyerOrderNumber = "00549425" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184510", BuyerOrderNumber = "00549447" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126655" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23126707" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000026091" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05952490" });
            list.Add(new EdiOrderInfo() { BuyerIln = "3024820007589", BuyerOrderNumber = "05507800" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184312", BuyerOrderNumber = "00549425" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900014184510", BuyerOrderNumber = "00549447" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000026166" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612928" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612929" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1612930" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1613130" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5904498000007", BuyerOrderNumber = "7000026147" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000754201", BuyerOrderNumber = "COMARCH1193870" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000889118", BuyerOrderNumber = "ZZ-159/04/23" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23129480" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23129493" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000930015", BuyerOrderNumber = "23129582" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5909000556713", BuyerOrderNumber = "C006831856" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1613277" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1613278" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1613282" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1613277" });
            list.Add(new EdiOrderInfo() { BuyerIln = "5900000065007", BuyerOrderNumber = "1613284" });


            #endregion

            string path = @"\\daxsyzyf.dax\INFINITE_FILES\ImpZam\PROCESSING\PROCESSED\";
            // string path = @"\\daxsyzyf.dax\Sprzedaz\EDI\impzam\PROCESSING\PROCESSED";

            DirectoryInfo di = new DirectoryInfo(path);
            foreach (var file in di.GetFiles("*.edx", SearchOption.TopDirectoryOnly))
            {
                DateTime treshold = DateTime.Today.AddDays(-30);
                if (file.CreationTime >= treshold)
                {
                    XmlDocument xd = new XmlDocument();

                    xd.Load(file.FullName);
                    var BuyerOrderNummber_Node = xd.SelectSingleNode("//BuyerOrderNumber");
                    var OrderNummber_Node = xd.SelectSingleNode("//OrderNumber");
                    var ILN_Node = xd.SelectSingleNode("//OrderParty/BuyerParty/ILN");
                    var Shipto_ILN_Node = xd.SelectSingleNode("//OrderParty/ShipToParty/ILN");
                    string BuyerOrderNummber = BuyerOrderNummber_Node.InnerText;
                    string OrderNummber = OrderNummber_Node.InnerText;
                    string ILN = ILN_Node.InnerText;
                    string ShipTo_ILN = Shipto_ILN_Node.InnerText;

                    if (list.Any(x => x.BuyerIln == ILN && x.BuyerOrderNumber == BuyerOrderNummber)) //&& x.OrderNumber == OrderNummber))
                    {
                        //file.CopyTo(Path.Combine(@"c:\1\1\1", file.Name));
                    }
                    else
                    {
                        file.CopyTo(Path.Combine(@"c:\1\1\1", file.Name));
                    }
                    xd = null;

                }

            }

        }
        [TestMethod]
        public void CalcCheckDigit()
        {
           
            string st = Dax_LabelPrinter.Hekpers.CheckSumCalc.CalcGs1_Checksum_Digit("590052505777");
            Assert.AreEqual(st.Length, 1);
        }
        class EdiOrderInfo
        {
            public string BuyerIln { get; set; }
            public string BuyerOrderNumber { get; set; }
            public string OrderNumber { get; set; }
        }
        class DocumentInfo
        {
            public int posId { get; set; }
            public string username { get; set; }
            public string kodAsort { get; set; }
            private FileInfo GetUniqueFileName(string path)
            {
                FileInfo fi = new FileInfo(path);

                for (int i = 1; ; ++i)
                {


                    if (!fi.Exists) return fi;
                    string TargetNewFileName = Path.GetFileNameWithoutExtension(fi.FullName);
                    string TargetNewFileExt = Path.GetExtension(fi.FullName);
                    string TargetNewPAth = Path.GetDirectoryName(fi.FullName);
                    fi = new FileInfo(Path.Combine(TargetNewPAth, String.Format("{0}_{1}{2}", TargetNewFileName, i, TargetNewFileExt)));

                }
            }
            public void Download()
            {

                const string cFilePathMain = @"\\dax\plik\PlikiNav\zapasy\DAXPRODUKCJA";
                string FileType;
                string FileName;
                long FileLength;
                int idRecord;
                string token;
                DaxFileStreamSrv.FileAssociacionType type;
                System.IO.Stream FileContent;

                int? IdPos = null;

                using (DaxFileStreamSrv.StreamingServiceClient client = new DaxFileStreamSrv.StreamingServiceClient())
                {
                    FileName = client.DownloadFile(posId, out FileType, out FileLength, out IdPos, out idRecord, out token, out type, out FileContent);

                }

                System.IO.FileInfo fi = new System.IO.FileInfo(FileName);

                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Path.Combine(cFilePathMain, this.kodAsort));
                if (!di.Exists) di.Create();
                System.IO.FileInfo fiTarget = new System.IO.FileInfo(System.IO.Path.Combine(
                    di.FullName, fi.Name));
                fiTarget = GetUniqueFileName(fiTarget.FullName);


                using (System.IO.FileStream localFile = new System.IO.FileStream(
                     fiTarget.FullName, System.IO.FileMode.CreateNew))
                {
                    //Response.BinaryWrite(archivedFile.BinaryData.ToArray());
                    const int bufferLen = 65000;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;

                    count = FileContent.Read(buffer, 0, bufferLen);
                    while (count > 0)
                    {
                        localFile.Write(buffer, 0, count);
                        count = FileContent.Read(buffer, 0, bufferLen);
                    }
                }
            }




        }

        [TestMethod]
        public void checkBlobField()
        {




            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Data Source=daxr2;Initial Catalog=nd;Integrated Security=True;MultipleActiveResultSets=True"))
            {
                conn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(
                    @"select Value from [Dax$Item Translation] it where it.[Item No_] = '010210151'",
                    conn))
                {
                    System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        byte[] arr = (byte[])dr["Value"];
                        var str = System.Text.Encoding.UTF8.GetString(arr);

                    }

                }

            }
        }


        [TestMethod]
        public void SpedCustExportTest()
        {
            SpedCustDaxLib.PackagesExport.Run();

        }

    }
}
