﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace QueryParserLib
{
    public class ParametersParser
    {
        public static List<string> GetParameterNames(string queryString)
        {
            string pattern = @"(?<whole>@((?<param>[0-9a-zA-Z_-]+)))";
            Regex regex = new Regex(pattern);
            List<string> retList = new List<string>();

            foreach (Match m in regex.Matches(queryString))
            {
                string paramName = m.Groups["param"].Value;
                if (!String.IsNullOrEmpty(paramName)) retList.Add(paramName);

            }
            return retList;
        }
        public static string ProcesNumSeries(string queryString)
        {
            string pattern = @"(?<whole>@!NumSeries!((?<param>[ 0-9a-zA-Z_-]+)))";
            Regex regex = new Regex(pattern);
           

            foreach (Match m in regex.Matches(queryString))
            {
                string paramName = m.Groups["param"].Value;
                using (DaxSrv_Produkcja.ProdukcjaCodeunit_PortClient client = new DaxSrv_Produkcja.ProdukcjaCodeunit_PortClient())
                {
                    var val = client.GetNextSeriesNo(paramName);
                    queryString = Regex.Replace(queryString, m.Value, val.Trim());
                }
            }
            return queryString;
        }

    }
}
