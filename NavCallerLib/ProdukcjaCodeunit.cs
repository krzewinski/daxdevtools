﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.ServiceModel;

namespace NavCallerLib
{
    public class ProdukcjaCodeunit
    {
        public DateTime CalcDate(string DateFormula)
        {
            using (Srv_ProdukcjaCodeunit.ProdukcjaCodeunit_PortClient client =
                (Srv_ProdukcjaCodeunit.ProdukcjaCodeunit_PortClient)FactoryManager.ProdukcjaFactory.GetClient())
            {
                return client.CalcDate(DateFormula);
            }



        }
    }

    public static class FactoryManager
    {
        static ProdCodeunit_Factory produkcjaFactory = new ProdCodeunit_Factory();

        internal static ProdCodeunit_Factory ProdukcjaFactory { get => produkcjaFactory; set => produkcjaFactory = value; }
    }
    public abstract class Client_Facotry<T> where T : class
    {
        public abstract System.ServiceModel.ClientBase<T> GetClient();


    }
    internal class ProdCodeunit_Factory : Client_Facotry<NavCallerLib.Srv_ProdukcjaCodeunit.ProdukcjaCodeunit_Port>
    {

        public override System.ServiceModel.ClientBase<NavCallerLib.Srv_ProdukcjaCodeunit.ProdukcjaCodeunit_Port> GetClient()
        {




            var binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            binding.ReceiveTimeout = new TimeSpan(0, 360, 0);
            binding.SendTimeout = new TimeSpan(0, 360, 0);


            var endpointAddress = new EndpointAddress(@"http://daxsql2k801.dax:7047/Dax/WS/Dax/Codeunit/AgentJobs");

            //var channelFactory = new ChannelFactory<NavCallerLib.Srv_ProdukcjaCodeunit.ProdukcjaCodeunit_Port>(binding, endpointAddress);
            var clientBase = new Srv_ProdukcjaCodeunit.ProdukcjaCodeunit_PortClient(binding, endpointAddress);

            System.ServiceModel.Description.ClientCredentials delegation = new System.ServiceModel.Description.ClientCredentials();
            delegation.SupportInteractive = true;
            delegation.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Identification;
            delegation.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Delegation;
            delegation.Windows.AllowNtlm = true;
            //channelFactory.Endpoint.Behaviors.Clear();
            //channelFactory.Endpoint.Behaviors.Add(delegation);
            clientBase.Endpoint.EndpointBehaviors.Clear();
            clientBase.Endpoint.EndpointBehaviors.Add(delegation);

            return clientBase;
             



        }
    }
}